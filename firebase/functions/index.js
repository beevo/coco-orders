const functions = require('firebase-functions');
require('dotenv').config()
const { google } = require("googleapis");

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

// Take the text parameter passed to this HTTP endpoint and insert it into the
// Realtime Database under the path /messages/:pushId/original
exports.addMessage = functions.https.onRequest(async (req, res) => {
  // Grab the text parameter.
  const original = req.query.text;
  // Push the new message into the Realtime Database using the Firebase Admin SDK.
  const snapshot = await admin.database().ref('/messages').push({ original: original });
  // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
  res.redirect(303, snapshot.ref.toString());
});

const FAKE_ORDER = {
  fname: "Bee",
  lname: 'Vo',
  items: [
    {
      id: 1,
      qty: 5
    },
    {
      id: 3,
      qty: 3
    }
  ],
  method: 'delivery'
};

// https://us-central1-coco-orders.cloudfunctions.net/submitOrder
exports.submitOrder = functions.https.onRequest(async (req, res) => {
  const auth = await google.auth.getClient({
    keyFile: 'keyfile.json',    
    scopes: [
      "https://www.googleapis.com/auth/spreadsheets"
    ]
  });

  // const order = FAKE_ORDER;

  // const sheetsAPI = google.sheets({ version: "v4", auth });
  // const snapshot = await admin.database().ref('/orders').push(order);
  // const orderKey = snapshot.key;
  // const invoiceSheetId = await addEmptySheet(sheetsAPI, `Invoice-${orderKey}`);

  // await populateAndStyle(sheetsAPI, order, invoiceSheetId);
  res.status(200).send({
    // order,
    // invoiceSheetId,
    // orderKey
    status: 'ok'
  });
});

// Listens for new messages added to /messages/:pushId/original and creates an
// uppercase version of the message to /messages/:pushId/uppercase
exports.makeUppercase = functions.database.ref('/messages/{pushId}/original')
  .onCreate((snapshot, context) => {
    // Grab the current value of what was written to the Realtime Database.
    const original = snapshot.val();
    console.log('Uppercasing', context.params.pushId, original);
    console.log(context.params);
    const upVal = original.toUpperCase();
    // You must return a Promise when performing asynchronous tasks inside a Functions such as
    // writing to the Firebase Realtime Database.
    // Setting an "uppercase" sibling in the Realtime Database returns a Promise.
    return snapshot.ref.parent.child('uppercase').set(upVal);
  });

// Creates a new sheet in the spreadsheet with the given name at position 2,
// with 26 colums and 2000 rows with the first row frozen.
// Returns its sheetId
function addEmptySheet(sheetsAPI, sheetName) {
  return new Promise((resolve, reject) => {
    const addEmptySheetParams = {
      // reading SHEET_ID from function environment variable
      spreadsheetId: process.env.ORDERS_AND_INVOICES_SHEET_ID,
      resource: {
        requests: [
          {
            addSheet: {
              properties: {
                title: sheetName,
                index: 1,
                gridProperties: {
                  rowCount: 100,
                  columnCount: 26,
                  frozenRowCount: 1
                }
              }
            }
          }
        ]
      }
    };
    console.log("anc");
    sheetsAPI.spreadsheets.batchUpdate(addEmptySheetParams, (
      err,
      response
    ) => {
      if (err) {
        reject(new Error("The Sheets API returned an error: " + err));
      } else {
        const sheetId = response.data.replies[0].addSheet.properties.sheetId;
        console.log("Created empty sheet: " + sheetId);
        resolve(sheetId);
      }
    });
  });
}

function populateAndStyle(sheetsAPI, order, sheetId) {
  return new Promise((resolve, reject) => {

    // let values = [
    //   ['foo', 'bar'],
    //   ['fit', 'bit'],
    //   [order.fname, order.lname]
    // ]
    // const data = [
    //   { range, values }
    // ];
    // Using 'batchUpdate' allows for multiple 'requests' to be sent in a single batch.
    // Populate the sheet referenced by its ID with the data received (a CSV string)
    // Style: set first row font size to 11 and to Bold. Exercise left for the reader: resize columns
    const dataAndStyle = {
      spreadsheetId: process.env.ORDERS_AND_INVOICES_SHEET_ID,
      resource: {
        requests: [
          // {
          //   pasteData: {
          //     coordinate: {
          //       sheetId: sheetId,
          //       rowIndex: 0,
          //       columnIndex: 0
          //     },
          //     data: theData,
          //     delimiter: ","
          //   }
          // },
          {
            updateCells: {
              "rows": [
                ["a", "b"],
                ["c", "d"]
              ],
              start: {
                sheetId: sheetId,
                rowIndex: 0,
                columnIndex: 0
              }
            }
          },
          {
            repeatCell: {
              range: {
                sheetId: sheetId,
                startRowIndex: 0,
                endRowIndex: 1
              },
              cell: {
                userEnteredFormat: {
                  textFormat: {
                    fontSize: 11,
                    bold: true
                  }
                }
              },
              fields: "userEnteredFormat(textFormat)"
            }
          }
        ]
      }
    };

    sheetsAPI.spreadsheets.batchUpdate(dataAndStyle, function (err, response) {
      if (err) {
        reject(new Error("The Sheets API returned an error: " + err));
      } else {
        console.log(sheetId + " sheet populated with " + theData.length + " rows and column style set.");
        resolve();
      }
    });
  });
}